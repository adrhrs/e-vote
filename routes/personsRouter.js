const express = require('express');
const router = express.Router();
const personController = require('../app/api/controllers/person');

router.get('/', personController.person);
router.get('/:user_id', personController.findPerson);
router.post('/', personController.createPerson);
router.put('/', personController.updatePerson);
router.delete('/', personController.deletePerson);


module.exports = router;