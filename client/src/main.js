import Vue from 'vue'
import App from './App.vue'
import router from './router'


window.backend = 'http://127.0.0.1:3000/';

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
