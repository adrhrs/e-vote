'use strict';

exports.ok = function(values, res) {
  var data = {
      'status': 200,
      'msg' : 'success',
      'values': values
  };
  res.json(data);
  res.end();
};

exports.fail = function(res) {
  var data = {
      'status': 404,
  };
  res.json(data);
  res.end();
};