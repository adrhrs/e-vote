const express = require('express');
const logger = require('morgan');

const cluster = require('cluster');
const http = require('http');
const numCPUs = require('os').cpus().length;

const users = require('./routes/users');
const persons = require('./routes/personsRouter');
const vote = require('./routes/voteRouter');

const bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');

const app = express();



app.set('secretKey', 'adrian'); 

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", 
    "Origin, X-Requested-With, x-access-token, Content-Type, Accept");
  next();
});

app.use(logger('dev'));
app.use(bodyParser.urlencoded({
    extended: false
}));

app.get('/', function(req, res) {
    res.json({
        "tutorial": "Build REST API with node.js"
    });
});

app.use('/users', users);
app.use('/persons', validateUser, persons);
app.use('/vote', validateUser, vote);

app.get('/favicon.ico', function(req, res) {
    res.sendStatus(204);
});

app.use(function(req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use(function(err, req, res, next) {
    console.log(err);

    if (err.status === 404)
        res.status(404).json({
            message: "Not found"
        });
    else
        res.status(500).json({
            message: "Something looks wrong :( !!!"
        });
});

const server = app.listen(3000, function() {
    console.log('Node server listening on port 3000');
    console.log(`Master ${process.pid} is running`);
    console.log(numCPUs)
});



const io = require('socket.io')(server);

app.locals.io = io

io.on('connection', function(socket) {
    
    socket.on('SEND_MESSAGE', function(data) {
        io.emit('MESSAGE', data)

    });

});

function validateUser(req, res, next) {

    jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded) {


        if (err) {
            res.json({
                status: "error",
                message: err.message,
                data: null
            });
        } else {
            req.body.userId = decoded.id;
            // console.log(req.body.userId)
            next();
        }
    });

}
