const express = require('express');
const router = express.Router();
const voteCon = require('../app/api/controllers/vote');

router.post('/', voteCon.storeVote);
router.get('/result', voteCon.getResult);

module.exports = router;