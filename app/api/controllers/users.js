const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

var connection = require('../config/db');

var conf = connection.config
var sql = connection.sql

var response = require('../config/res');

module.exports = {

    testApi: function(req, res, next) {

        var p = 1
        var m = Math.floor((Math.random() * 6000000) + 7000000);

        for (var i = 0; i < m; i++) {
            p += i
        }

        response.ok(p, res)

    },

    newCreate: function(req, res, next) {

        var name = req.body.name;
        var email = req.body.email;
        var password = req.body.password;

        console.log(req.body)


        sql.connect(conf, function(err) {

            if (err) console.log(err);
            var request = new sql.Request();

            request.query("INSERT INTO USERS (name, email, password) VALUES ('" + name + "','" + email + "','" + password + "')",

                function(err, rows) {

                    if (err) console.log(err)

                    sql.close()

                    response.ok("Berhasil mendaftarkan akun login !", res)


                });
        });

    },

    delete: function(req, res, next) {

        var id = req.body.id;

        console.log("delete controller")
        console.log(req.body.id)


        sql.connect(conf, function(err) {

            if (err) console.log(err);
            var request = new sql.Request();

            request.query("DELETE FROM USERS WHERE ID='" + id + "' ",

                function(err, rows) {

                    if (err) console.log(err)

                    sql.close()

                    response.ok("Berhasil menghapus data !"+req.body.id, res)


                });
        });

    },

    edit: function(req, res, next) {

        var id = req.body.id;
        var name = req.body.name;
        var email = req.body.email;
        var password = req.body.password;

        sql.connect(conf, function(err) {

            if (err) console.log(err);
            var request = new sql.Request();

            request.query("UPDATE USERS SET name='" + name + "',email='" + email + "',password='" + password + "' WHERE ID='" + id + "'",

                function(err, rows) {

                    if (err) console.log(err)

                    sql.close()

                    response.ok("Berhasil mengupdate data !"+id, res)


                });
        });

    },

    newGet: function(req, res, next) {


        sql.connect(conf, function(err) {

            if (err) console.log(err);
            var request = new sql.Request();

            request.query('SELECT * FROM USERS', function(err, rows) {

                if (err) console.log(err)

                sql.close()
                response.ok(rows.recordset, res)


            });
        });


    },

    newAuth: function(req, res, next) {

        sql.connect(conf, function(err) {

            if (err) console.log(err);
            var request = new sql.Request();

            var email = req.body.email;
            var password = req.body.password;

            request.query("SELECT * FROM USERS WHERE email = '"+email+"' AND password = '"+password+"'", function(err, rows) {

                if (err) console.log(err)

                sql.close()

                if(rows.recordset.length == 1) {

                    idx = rows.recordset[0].id


                    const token = jwt.sign({id : idx}, req.app.get('secretKey'), { expiresIn: '1h' });

                    var data = [{
                        user: rows.recordset,
                        token: token
                    }]

                    response.ok(data, res)

                } else {



                    response.fail(res)
                }

            });
        });


    }

}