var connection = require('../config/db');

var conf = connection.config
var sql = connection.sql

var response = require('../config/res');

module.exports = {

    person: function(req, res, next) {

        sql.connect(conf, function(err) {

            if (err) console.log(err);
            var request = new sql.Request();

            request.query('SELECT * FROM PERSON', function(err, rows) {

                if (err) console.log(err)

                sql.close()
                response.ok(rows.recordset, res)


            });
        });
    },

    findPerson: function(req, res, next) {

        var user_id = req.params.user_id;

        sql.connect(conf, function(err) {

            if (err) console.log(err);
            var request = new sql.Request();

            request.query("SELECT * FROM PERSON WHERE id ='"+user_id+"'", function(err, rows) {

                if (err) console.log(err)

                sql.close()
                response.ok(rows.recordset, res)


            });
        });

    },

    

    createPerson: function(req, res, next) {

        var first_name = req.body.first_name;
        var last_name = req.body.last_name;

        console.log(first_name);
        console.log(last_name);

        sql.connect(conf, function(err) {

            if (err) console.log(err);
            var request = new sql.Request();

            request.query("INSERT INTO person (first_name, last_name) VALUES ('" + first_name + "','" + last_name + "')",

                function(err, rows) {

                    if (err) console.log(err)

                    sql.close()

                    response.ok("Berhasil mendaftarkan person !", res)


                });
        });
    },

    updatePerson: function(req, res, next) {

        var user_id = req.body.user_id;
        var first_name = req.body.first_name;
        var last_name = req.body.last_name;

        sql.connect(conf, function(err) {

            if (err) console.log(err);
            var request = new sql.Request();

            request.query("UPDATE person SET first_name = '" + first_name + "', last_name = '" + last_name + "' WHERE id = '"+ user_id +"'",

                function(err, rows) {

                    if (err) console.log(err)

                    sql.close()

                    response.ok("Berhasil mengupdate person !", res)


                });
        });

    },

    deletePerson: function(req, res, next) {

        var user_id = req.body.user_id;

        sql.connect(conf, function(err) {

            if (err) console.log(err);
            var request = new sql.Request();

            request.query("DELETE FROM person WHERE id = '" + user_id + "'",

                function(err, rows) {

                    if (err) console.log(err)

                    sql.close()

                    response.ok("Berhasil menghapus person !", res)


                });
        });

    }



}