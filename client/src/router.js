import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const auth = (to, from, next) => {
  
  if(!localStorage.getItem('jwt')) {

  
    next('/login')

  } else {
    next()
  }

  
}

const loginguard = (to, from, next) => {
  
  if(!localStorage.getItem('jwt')) {

    next()

  } else {

    next(false)

  }
  
}

const logout = (to, from, next) => {

  localStorage.clear()
  next('/login')

}

export default new Router({
  mode: 'history',
  routes: [
    
    {
      path: '/logout',
      name: 'logout',
      beforeEnter: logout
    },
    
    {
      path: '/',
      name: 'form',
      component: () => import(/* webpackChunkName: "about" */ './views/Form.vue'),
      beforeEnter: auth
    },

    {
      path: '/chat',
      name: 'chat',
      component: () => import(/* webpackChunkName: "about" */ './views/Chat.vue'),
      beforeEnter: auth
    },

    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: "about" */ './views/Login.vue'),
      beforeEnter: loginguard
    },

    {
      path: '/person',
      name: 'person',
      component: () => import(/* webpackChunkName: "about" */ './views/Person.vue'),
      beforeEnter: auth
    },

    {
      path: '/vote',
      name: 'vote',
      component: () => import(/* webpackChunkName: "about" */ './views/Vote.vue'),
      beforeEnter: auth
    }

    

  ]
})
