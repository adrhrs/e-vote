const express = require('express');
const router = express.Router();
const userController = require('../app/api/controllers/users');


router.post('/newRegister', userController.newCreate);
router.post('/delete', userController.delete);
router.post('/edit', userController.edit);
router.post('/auth', userController.newAuth);
router.get('/', userController.newGet);
router.get('/test', userController.testApi);



module.exports = router;