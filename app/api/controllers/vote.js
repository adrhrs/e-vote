var connection = require('../config/db');

var conf = connection.config
var sql = connection.sql

var response = require('../config/res');

module.exports = {

    getResult: function(req, res, next) {


        sql.connect(conf, function(err) {

            if (err) console.log(err);
            var request = new sql.Request();

            request.query('SELECT no_pilihan as paslon, count(*) as total FROM VOTE_RESULT GROUP BY no_pilihan', function(err, rows) {

                if (err) console.log(err)

                sql.close()
                response.ok(rows.recordset, res)


            });
        });


    },

    storeVote: function(req, res, next) {

        var option = req.body.option;

        sql.connect(conf, function(err) {

            if (err) console.log(err);
            var request = new sql.Request();

            request.query("INSERT INTO VOTE_RESULT (no_pilihan) VALUES ('" + option + "')",

                function(err, rows) {

                    if (err) console.log(err)

                    sql.close()
                
                    response.ok("Berhasil submit vote !", res)
                    
                    const io = req.app.locals.io

                    var cek = {

                        action: 'Vote ',
                        value: option
                    }

                    // console.log(cek)

                    io.emit('VOTE', cek)
                    

                });
        });
    },



}